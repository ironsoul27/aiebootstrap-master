#include "Application2D.h"

int main() {
	
	// allocation
	auto app = new Application2D();

	// initialise and loop
	app->run("AIE", 1280, 720, false);

	//aie::Renderer2D
	// deallocation
	delete app;

	return 0;
}
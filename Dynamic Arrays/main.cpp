#include "Dynamic_ArraysApp.h"

int main() {
	
	// allocation
	auto app = new Dynamic_ArraysApp();

	// initialise and loop
	app->run("AIE", 1280, 720, false);

	// deallocation
	delete app;

	return 0;
}
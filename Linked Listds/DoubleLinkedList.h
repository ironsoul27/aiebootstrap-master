#pragma once
#include <assert.h>

using std::cout;
using std::endl;


template <typename T>
class DoubleLinkedList
{
private:
	class Node
	{
	public:
		Node(T data, Node* next = nullptr, Node* previous = nullptr)  // T data is the 'type' of data to pass through e.g int, float, and holds the value that is passed through, which contains the data that the node will hold // use constructor when creating node with custom values
		{
			this->data = data; // this-> pointer to this classes data variable
			this->next = next;
			this->previous = previous;
		}

		T data; // T to change based on data type used/passed through   // when the arrays are called they initialise the template to int's so whenever they are called for a function T becomes int
		Node* next; // *returns pointers to all the data types inside that class etc. data contains whole values while next/previous return as pointers only
		Node* previous;
	};

public:
	// Iterator used to step through the collection
	// Can be used outside the class, so we make it public

	class Iterator // like a briefcase that keeps the current selected node, then offers functions to change the contents of the briefcase, without ever touching it directly, in order to avoid corrupting the data.
	{
	public:
		// Default constructor
		Iterator(Node* node = nullptr) // if nothing was passed through node becomes a nullptr
		{
			this->node = node; // set the classes local node's value in the list to equal the node passed through  e.g pointers
		}

		bool isValid() const // check if node exists
		{
			return (node != nullptr);
		}

		T& getData() const 
		{
			if (node == nullptr)
			{
				throw "Invalid iterator";
			}
			return node->data;
		}

		bool moveNext()
		{
			if (node != nullptr) // if the node exists
			{
				node = node->next; // node becomes a pointer to the next node
			}

			return isValid(); // return isvalid to check if this new node returns anything, else it has reached the end of the list
		}

		bool movePrevious()
		{
			if (node != nullptr)
			{
				node = node->previous; // same here but just for the previous
			}

			return isValid();
		}


	private:
		friend class DoubleLinkedList;
		Node* node; // node doesnt lose its data upon exiting of class functions since it acts as a global variable for that class, contains the currently selected node
	};


public:

	DoubleLinkedList()
	{
		head = tail = nullptr; // set head and tail to point to nothing
	}

	~DoubleLinkedList()
	{
		clear();
	}


	bool empty() const
	{
		return (head == nullptr) || (tail == nullptr);
	}


	void clear()
	{
		Node* node = head; // new node that contains heads pointer to the current nodes head
		Node* nodeToDelete;

		while (node != nullptr) // while node exists
		{
			nodeToDelete = node;
			node = node->next;
			delete nodeToDelete;
		}
		head = tail = nullptr;
	}


	void pushFront(const T& data) // front of list
	{
		
		Node* newNode = new Node(data, head);

		if (head != nullptr)
		{
			head->previous = newNode;
		}
		head = newNode;


		if (tail == nullptr)
		{
			tail = newNode;
		}
	}


	void pushBack(const T& data) 
	{
		Node* newNode = new Node(data, nullptr, tail); // newNode creates a new node which holds the data variable passed through by the player, and contains the tails pointer to the previous node behind it

		if (tail != nullptr) // if tail doesnt point to anything
		{
			tail->next = newNode;
		}
		tail = newNode;

		if (head == nullptr)
		{
			head = newNode;
		}
	}


	void insert(Iterator& iter, const T& data) {

		if (empty())
		{
			pushFront(data); 
		}
		else
		{
			Node* insertNode = iter.node; 
			if (insertNode != nullptr)
			{
				Node* newNode = new Node(data, insertNode->next, insertNode);
				if (insertNode->next != nullptr)
				{ 
					insertNode->next->previous = newNode; // the pointer for what is the previous pointer of the next node will point to this new node
				}
				insertNode->next = newNode;

				if (tail == insertNode) 
				{
					tail = newNode;
				} 
			}
			else {
				throw "Array is empty";
			}
		} 
	}


	void erase(Iterator& iter) const
	{
		if (empty())
		{
			throw "Array is already empty";
		}
		else {
			iter.node->next->previous = iter.node->previous;
			iter.node->previous->next = iter.node->next;
			delete iter.node;
		}
	}


	void remove(const T& data)
	{
		Iterator it;
		Node* n = begin();
		while (tail != nullptr)
		{
			if (n == nullptr)
			{
				throw "Array is empty";
			}
			else if (n->data == data){
				it = n;
				erase(it);
			}
		}
	}

	
	T popFront() // T holds the type of data that is passed through when creating data, also holds the variable 'data' which is created in node
	{
		if (head == nullptr)
		{
			throw "List is empty";
		}

		Node* n = head;

		if (head == tail) // if head == tail (we no longer have any nodes left)
		{
			head = tail = nullptr; // set Head/Tail to nullptr
		}
		else {
			head = head->next; // head = head->next's value, which will be holding the pointer to the next node
			head->previous = nullptr;
		}

		T data = n->data; // Creating new data which will hold

		delete n;

		return data;
	}


	T popBack()
	{
		if (tail == nullptr)
		{
			throw "List is empty";
		}

		Node* n = head;

		if (head == tail)
		{
			head = tail = nullptr;
		}
		else {
			tail = tail->previous;
			tail->next = nullptr;
		}

		T data = n->data;  // Create new data which holds the data pointed to by n

		delete n;

		return data;
	}


	T&  first() const
	{
		if (head == nullptr)
		{
			assert(head == nullptr && "Head does not point to anything, list is empty");

		}
		return head->data; // return the data held inside the pointer address by tail

		//if (head == nullptr)
		//{
		//	assert(tail == nullptr && "Head does not point to anything, list is empty");
		//	//throw "List is empty";
		//}
		//return head->data;
	
	}


	T& last() const
	{
		if (tail != nullptr)
		{
			return tail->data; // return the data held inside the pointer address by tail
		}
		else {
			assert(tail == nullptr && "Tail does not point to anything, list is empty");
		}
	}


	Iterator begin() const
	{
		return Iterator(head); // calls default constructor passing through heads pointer
	}


	Iterator end() const
	{
		Node* n = tail;

		if (tail->next == nullptr)
		{
			n = tail->next;
		}
		else {
			throw "Error, List is empty";
		}
		return Iterator(n);
	}


	unsigned int count() const
	{	
		unsigned int counter = 0;
		Node* node = head;
		while (node != nullptr)
		{
			counter++;
			node = node->next;
		}
		return counter;
	}


	void print() const
	{
		Node* node = head;

		while (node != nullptr)
		{
			T data = node->data;		
			cout << data << endl;
			node = node->next;
		}
	}




private:
	Node* head; // *allows the code to realise that it is looking at a nullptr since it is initialising head with it's data, which consists of pointers, and initializing head to be a pointer as well      and can work with that scenario, as opposed to & which can't // head and tail point to the actual values themselves and their positions as opposed to their connecting pointers.
	Node* tail;
};
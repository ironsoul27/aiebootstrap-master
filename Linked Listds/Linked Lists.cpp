#include "pch.h"
#include <iostream>
#include "DoubleLinkedList.h"
#include "Linked Lists.h"
#include <string>
#include <vector>
#include <assert.h>

using std::cout;
using std::endl;
using std::cin;
using std::string;

std::vector<Command> commands;

void printCommand(std::string data)
{
	cout << "Print:" << data << endl;
}

void addCommand(std::string data)
{
	cout << "Add:" << data << endl;
}

void routeCommand(std::string commandName, std::string data)
{
	for (unsigned int i = 0; i < commands.size(); i++)
	{
		if (commands[i].commandName == commandName)
		{
			commands[i].commandFunction(data);
			return;
		}
	}
}


int main()
{

	commands.push_back({ "print", printCommand });
	commands.push_back({ "add", addCommand});

	std::string commandName = "print";
	std::string commandData = "hello world";

	routeCommand(commandName, commandData);


/*
	DoubleLinkedList<int> array;
	DoubleLinkedList<int>::Iterator it = array.begin(); // 'it' is a new variable which holds the Iterators value to the nodes
	if (it.isValid())
	{
		it.getData();
	}

	grabInput();
	result = checkInput(input);

	switch (result) {
		case CLEAR:
		{
			cout << "CLEAR" << endl;
			break;
		}
		case FAIL:
		{		
			assert(FAIL && "Incorrect player input");
		}
	}



	array.first();
	array.pushFront(2);
	array.clear();
	array.pushFront(23);
	array.pushFront(14);

	

	array.pushBack(5);
	array.popFront();

	i = array.count();

	it = array.begin();
	array.insert(it, 23);
	it.moveNext();
	
	l = array.first();
	array.popFront();
	array.print();
	cout << "\n" << endl;

	cout << l << endl;
	cout << i << endl;
*/
	return 0;
}

//void grab

unsigned int checkInput(string input)
{
	if (input == "hello") {
		return CLEAR;
	}

	return FAIL;
}
#pragma once
#include <string>
#include <functional>

using std::string;

unsigned int checkInput(string input);
unsigned int i;
unsigned int l;
string input;
unsigned int result;

struct Command
{
	std::string commandName;
	std::function<void(std::string data)> commandFunction;
};

enum returnType
{
	CLEAR = 1,
	FAIL
};
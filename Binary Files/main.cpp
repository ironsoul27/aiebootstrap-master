#include "Binary_FilesApp.h"
//#include "Binary Files.h"

int main() {
	
	// allocation
	auto app = new Binary_FilesApp();
	extern aie::Font* g_systemFont;
	// initialise and loop
	app->run("AIE", 1280, 720, false);

	// deallocation
	delete app;

	return 0;
}
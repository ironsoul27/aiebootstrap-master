#pragma once
#include "Binary_FilesApp.h"

//template <typename T>
class TreeNode {
public:

	//TreeNode(TreeNode* left = nullptr, TreeNode* right = nullptr) {
	//	this->left = left;
	//	this->right = right;
	//};


	TreeNode(int value)
	{
		//this->m_value = m_value;
	};
	~TreeNode() {};

	bool hasLeft() { return (left != nullptr); }
	bool hasRight() { return (right != nullptr); }

	int getData() { return m_value; }
	TreeNode* getLeft() { return left; }
	TreeNode* getRight() { return right; }

	void setData(int m_value) { m_value = m_value; }
	void setLeft(TreeNode* node) { left = node; }
	void setRight(TreeNode* node) { right = node; }

	void draw(aie::Renderer2D*, int x, int y, bool selected = false);

	static aie::Font* g_systemFont;

public:
	

private:
	int m_value;
	TreeNode* left = nullptr;
	TreeNode* right = nullptr;

};
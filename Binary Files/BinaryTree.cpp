#include "BinaryTree.h"
#include "TreeNode.h"

BinaryTree::BinaryTree() {};

BinaryTree::~BinaryTree() {};


void BinaryTree::draw(aie::Renderer2D* renderer, TreeNode* selected)
{
	draw(renderer, m_pRoot, 640, 680, 640, selected);
}

bool BinaryTree::isEmpty() const {

	return m_pRoot == nullptr;

}

void BinaryTree::insert(int a_nValue)
{
	TreeNode* Node = new TreeNode(a_nValue);

	if (m_pRoot == nullptr)
	{
		m_pRoot->setRight(Node);
	}
	while (c_Node->getLeft() != nullptr && c_Node->getRight() != nullptr) {
		if (a_nValue < c_Node->getData()) {
			c_Node = c_Node->getLeft();
		}
		if (a_nValue > c_Node->getData()) {	
			c_Node = c_Node->getRight();
		}
		if (a_nValue == c_Node->getData()) {
			break;
		}
	}
	if (a_nValue < c_Node->getData()) {
		c_Node->setLeft(Node);
	}
	else {
		c_Node->setRight(Node);
	}

	//delete Node;
}


void BinaryTree::draw(aie::Renderer2D* renderer, TreeNode* pNode, int x, int y,
						int horizontalSpacing, TreeNode* selected)
{
	horizontalSpacing /= 2;

	if (pNode) {
		if (pNode->hasLeft()) {
			renderer->setRenderColour(1, 0, 0);
			renderer->drawLine(x, y, x - horizontalSpacing, y - 80);
			draw(renderer, pNode->getLeft(), x - horizontalSpacing,
					y - 80, horizontalSpacing, selected);
		}

		if (pNode->hasRight()) {
			renderer->setRenderColour(1, 0, 0);
			renderer->drawLine(x, y, x + horizontalSpacing, y - 80);
			draw(renderer, pNode->getRight(), x + horizontalSpacing,
				y - 80, horizontalSpacing, selected);
		}

		pNode->draw(renderer, x, y, (selected == pNode));
	}
}
